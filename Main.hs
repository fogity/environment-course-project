{-# LANGUAGE LambdaCase, TupleSections #-}

import Control.Arrow (second)
import Data.List (nub, partition, sortOn)
import Data.Map (Map)
import qualified Data.Map as M
import Data.Maybe (fromJust)
import System.Environment (getArgs)

-- | Representerar en nod i grafen.
type Node       = String

-- | Representerar en kant i grafen.
type Edge       = (Node, Node)

-- | Representerar värden i grafen.
type Amount     = Double

-- | Representerar effektivitet för en kant.
type Efficiency = Double

-- | Representerar en andel av en nods inkommande värde för en kant.
type Share      = Double

-- | Representerar tidsenheten.
type Year       = Double

-- | Representerar en rangorning av kanter.
type Priority   = Int

-- | Representerar hur en kant fördelas till en nod.
data Distribution
  -- | Beskriver att kanten tar in ett värde och ger ett värde.
  = Fixed Amount Amount
  -- | Beskriver att kanten står för en viss andel av värdet in i en nod.
  | Share Share
  -- | Beskriver att kanten står för en viss andel av resterande värdet in i en
    -- nod och ska väljas med en viss prioritet.
  | Fill Priority Share

-- | Information kopplat till en kant.
data EdgeData = ED
  { -- | Motsvarar verkningsgrad för kanten.
    conversion   :: Efficiency
  , -- | Motsvarar transmissionsförluster för kanten.
    transmission :: Efficiency
  , -- | Motsvarar hur kanten ska distibueras till noden.
    distribution :: Distribution
  , -- | Motsvarar en begränsning i kapacitet för kanten.
    capacity     :: Maybe (Amount, Amount)
  }

-- | Representation av grafen innen beräkningar.
data InputGraph = InputGraph
  { -- | Noder med behov.
    demands    :: Map Node Amount
  , -- | Noder med begränsad kapacitet.
    capacities :: Map Node Amount
  , -- | Kanterna i grafen.
    edges      :: Map Edge EdgeData
  }

-- | Representation av grafen efter beräkninger.
data OutputGraph = OutputGraph
  { -- | Noder och deras värden.
    outputNodes :: Map Node Amount
  , -- | Kanter och deras värden.
    outputEdges :: Map Edge (Amount, Amount)
  }

-- | Läser in och kör alla specifierade scenarion.
main :: IO ()
main = mapM_ (runScenario . (++ ".scenario")) =<< getArgs

-- | Kör en scenariofil.
runScenario :: FilePath -> IO ()
runScenario path = do
  f <- readFile path
  -- Ta bort alla tomma rader och ta bort allt efter '#' för varje rad.
  let ls = filter (not . null) . map (words . takeWhile (/= '#')) $ lines f
  case ls of
    ["modell", dataPath] : ["period", start, end, step] : ls' -> do
      input <- readInputGraph (dataPath ++ ".modell")
      let output = computeGraph input
          (subLs, postLs) = span ((== "|") . head) ls'
          a = read start
          b = read end
          s = read step
          d = 1 / s
          t = 1 - d
          years = concatMap (\y -> [y, y + d .. y + t]) [a .. b - 1] ++ [b]
          inputs = map (,input) years
          -- Sätter variabeln '0' till värdet 0.
          vars = M.fromList [("0", replicate (length inputs) 0)]
          n = length inputs
          subLs' = filter (not . null) $ map tail subLs
          (outputs, _) = runPeriod n inputs input output vars subLs'
      processResults (map frst outputs) (map thrd outputs) postLs
    _ -> error "första kommandot måste vara 'modell' andra 'period'"

-- | Läser in en graf från en modellfil.
readInputGraph :: FilePath -> IO InputGraph
readInputGraph path = do
  f <- readFile path
  -- Ta bort alla tomma rader och ta bort allt efter '#' för varje rad.
  let ls = filter (not . null) . map (words . takeWhile (/= '#')) $ lines f
      -- Dela upp raderna i noder och kanter.
      (nls, els) = partition (\l -> head l /= "kant") ls
      -- Dela upp noderna i behov och kapacitet.
      (dls, cls) = partition (\l -> head l == "behov") nls
      demands = M.fromList $ map parseDemand dls
      capacities = M.fromList $ map parseCapacity cls
      edges = M.fromList $ map parseEdge els
  return $ InputGraph demands capacities edges

-- | Läser behov.
parseDemand :: [String] -> (Node, Amount)
parseDemand ["behov", node, amount] = (node, read amount)
parseDemand ws = error $ "kan inte läsa behov: " ++ unwords ws

-- | Läser kapacitet.
parseCapacity :: [String] -> (Node, Amount)
parseCapacity ["kapacitet", node, amount, opt]
  | ("load", ':' : load) <- span (/= ':') opt
  = (node, read amount * read load)
  | otherwise = error $ "ogilitg kapacitetsparameter " ++ opt
parseCapacity ["kapacitet", node, amount] = (node, read amount)
parseCapacity ws = error $ "kan inte läsa kapacitet: " ++ unwords ws

-- | Läser en kant.
parseEdge :: [String] -> (Edge, EdgeData)
parseEdge ("kant" : from : to : dat) = ((from, to), parseEdgeData dat)
parseEdge ws = error $ "kan inte läsa kant: " ++ unwords ws

-- | Läser information för en kant.
parseEdgeData :: [String] -> EdgeData
parseEdgeData [] = ED 1 1 (Fill 0 1) Nothing
parseEdgeData (set : ws)
  | "fyll" <- set
  = dat { distribution = Fill 0 1 }
  | ("fyll", '[' : sets) <- span (/= '[') set
  = dat { distribution = parseFillData . splitOn ',' . init $ sets }
  | (opt, ':' : val) <- span (/= ':') set = case opt of
    "verk"  -> dat { conversion = read val }
    "trans" -> dat { transmission = read val }
    "andel" -> dat { distribution = Share $ read val }
    _ -> error $ "ogiltig kantparameter: " ++ opt
  | otherwise = error $ "ogiltig kantdata: " ++ set
  where dat = parseEdgeData ws

-- | Delar upp en sträng i strängar separerade av ett tecken.
splitOn :: Char -> String -> [String]
splitOn c s
  | null s'   = [w]
  | otherwise = w : splitOn c (tail s')
  where (w, s') = span (/= c) s

-- | Läser parametrar till parametern fyll för en kant.
parseFillData :: [String] -> Distribution
parseFillData [] = Fill 0 1
parseFillData (set : ws)
  | (opt, ':' : val) <- span (/= ':') set = case opt of
    "prio"  -> Fill (read val) s
    "andel" -> Fill p (read val)
    _ -> error $ "ogiltig fyllparameter: " ++ opt
  where (Fill p s) = parseFillData ws

-- | Beräknar faktiska värden från en graf med samband.
computeGraph :: InputGraph -> OutputGraph
computeGraph input = OutputGraph nodes edges''
  where
    -- Första steget är att propegera och beräkna kapaciteter för kanter.
    edges' = computeCapacities (capacities input) M.empty (edges input) M.empty
    -- Andra steget är att propegera och beräkna behov och värden.
    (nodes, edges'') = computeGraph' M.empty (demands input) M.empty edges' M.empty

-- | Beräknar och propegerar kapacitet för noder och kanter.
computeCapacities :: Map Node Amount -> Map Node Amount
                   -> Map Edge EdgeData -> Map Edge EdgeData
                   -> Map Edge EdgeData
computeCapacities fixedCapacity partialCapacity unused used
  | null fixedCapacity = unused `M.union` used
  | otherwise = let
    -- Tar valfri nod.
    (node, amount) = M.elemAt 0 fixedCapacity
    -- Tar alla kanter som går från noden.
    (current, unused') = M.partitionWithKey (\(f, _) -> const $ node == f) unused
    -- Uppdaterar kapacitet (antagande: endast en kant per kopplad nod).
    current' = fmap (\dat -> dat { capacity = Just (amount, amount * conversion dat * transmission dat) }) current
    -- Propagerar värden från kanterna till övriga ihopkopplade noder.
    partial = M.foldlWithKey (\ns (_, t) dat -> M.alter (let
      Just (a, _) = capacity dat in \case
        Nothing -> Just a
        Just a' -> Just $ a + a'
      ) t ns) partialCapacity current'
    -- Lista alla noder som har inkommande obegränsade kanter.
    nonFixed = nub . map snd $ M.keys unused'
    -- Tar ut alla noder som nu har begränsad kapacitet.
    (fixed, partial') = M.partitionWithKey (\n -> const $ n `notElem` nonFixed) partial
    -- Sammanställer alla noder med specifik kapacitet.
    fixed' = fixed `M.union` M.deleteAt 0 fixedCapacity
    -- Sammanställer alla kanter som har använts
    used' = current' `M.union` used
  in computeCapacities fixed' partial' unused' used'

-- | Beräknar och propegerar värden för noder och kanter.
computeGraph' :: Map Node Amount -> Map Node Amount -> Map Node Amount
              -> Map Edge EdgeData -> Map Edge (Amount, Amount)
              -> (Map Node Amount, Map Edge (Amount, Amount))
computeGraph' unfilledNodes filledNodes usedNodes unusedEdges usedEdges
  | null filledNodes = (usedNodes, usedEdges)
  | otherwise = let
    -- Tar valfri nod.
    (node, amount) = M.elemAt 0 filledNodes
    -- Tar alla kanter som leder till noden.
    (current, unused) = M.partitionWithKey (\(_, t) -> const $ node == t) unusedEdges
    -- Beräknar andel för en kant till noden.
    computeShare dat
      | Share s <- distribution dat
      , Just (x, y) <- capacity dat
      , y < s * amount
      = dat { distribution = Fixed x y }
      | Share s <- distribution dat
      = let
        y = s * amount
        x = y / conversion dat / transmission dat
      in dat { distribution = Fixed x y }
      | otherwise = dat
    -- Beräknar alla andelar till fixerade värden.
    current' = fmap computeShare current
    -- Extraherar utgående värde från en kant.
    extractAmount dat
      | Fixed _ y <- distribution dat = y
      | otherwise = 0
    -- Beräknar resterande andel.
    fill = amount - sum (fmap extractAmount current')
    -- Tar ut alla utfyllande kanter.
    (filling, rest) = M.partition (\dat -> case distribution dat of
      Fill _ _ -> True
      _ -> False
      ) current'
    -- Sortera utfyllande kanter efter prioritet.
    filling' = sortOn ((\dat -> let Fill x _ = distribution dat in x) . snd) $ M.toList filling
    -- Fördelar resterande andel mellan utfyllande kanter.
    distributeFill a ((edge, dat) : es)
      | Just (x, y) <- capacity dat
      , a * s > y
      = (edge, dat { distribution = Fixed x y }) : distributeFill (a - y) es
      | s /= 1
      = let
        b = a * s
        b' = b / conversion dat / transmission dat
      in (edge, dat { distribution = Fixed b' b }) : distributeFill (a - b) es
      | otherwise
      = let
        a' = a / conversion dat / transmission dat
        es' = map (\(e, dat) -> (e, dat { distribution = Fixed 0 0 })) es
      in (edge, dat { distribution = Fixed a' a }) : es'
      where
        Fill _ s = distribution dat
    distributeFill a []
      | a < 1e-12 = [] -- Potentiellt avrundningsfel
      | otherwise = error $ "kunde inte uppfylla allt behov för nod " ++ node ++ ", resterande: " ++ show a
    -- Uppdaterar utfyllande kant.
    filling'' = if null filling' then [] else distributeFill fill filling'
    -- Slår ihop alla aktuella kanter igen.
    current'' = M.fromList filling'' `M.union` rest
    -- Propagerar värden från kanterna till övriga ihopkopplade noder.
    unfilled = M.foldlWithKey (\ns (f, _) dat -> M.alter (let
      Fixed a _ = distribution dat in \case
        Nothing -> Just a
        Just a' -> Just $ a + a'
      ) f ns) unfilledNodes current''
    -- Tar fram alla noder som har utgående kanter.
    connected = nub . map fst $ M.keys unused
    -- Tar ut alla nyligen fyllda noder.
    (newlyFilled, unfilled') = M.partitionWithKey (\n -> const $ n `notElem` connected) unfilled
    -- Sammanställer alla fyllda noder.
    filled' = newlyFilled `M.union` M.deleteAt 0 filledNodes
    -- Markera behandlade noden som använd.
    usedNodes' = M.insert node amount usedNodes
    -- Markera behandlade kanter som använda.
    usedEdges' = usedEdges `M.union` fmap (\dat -> let Fixed x y = distribution dat in (x, y)) current''
  in computeGraph' unfilled' filled' usedNodes' unused usedEdges'

-- | Utför olika kommandon på modellen över en viss period.
runPeriod :: Int -> [(Year, InputGraph)] -> InputGraph -> OutputGraph
          -> Map String [Double] -> [[String]]
          -> ([(Year, InputGraph, OutputGraph)], Map String [Double])
runPeriod n inputs input output vars (l : ls)
  | ["var", name, start, end] <- l
  = let
    a = read start
    b = read end
    change = (b - a) / fromIntegral (n - 1)
    vals = [a, a + change .. b] ++ replicate (length inputs - n) b
    vars' = M.insert name vals vars
  in runPeriod n inputs input output vars' ls
  | ["behov", "alla", op, var] <- l
  , Just vals <- M.lookup var vars
  = let
    inputs' = zipWith (\v (y, i) -> (y,) $ adjustDemands (operator op v) i) vals inputs
  in runPeriod n inputs' input output vars ls
  | ["behov", node, op, var] <- l
  , Just vals <- M.lookup var vars
  = let
    inputs' = zipWith (\v (y, i) -> (y,) $ adjustDemand (operator op v) node i) vals inputs
  in runPeriod n inputs' input output vars ls
  | ["kapacitet", node, op, var] <- l
  , Just vals <- M.lookup var vars
  = let
    inputs' = zipWith (\v (y, i) -> (y,) $ adjustCapacity (operator op v) node i) vals inputs
  in runPeriod n inputs' input output vars ls
  | ["fixera", from, to] <- l
  = let
    inputs' = map (second $ fixEdge (from, to) output) inputs
  in runPeriod n inputs' input output vars ls
  | name : "=" : expr <- l
  = let
    vals = case expr of
      ["utgående", from, to] -> case M.lookup (from, to) $ outputEdges output of
        Just (_, y) -> replicate (length inputs) y
        Nothing     -> error $ "kanten " ++ show (from, to) ++ " finns inte"
      "konvertera" : var : "via" : path -> case M.lookup var vars of
        Just vs -> map (convertAmount path input) vs
        Nothing -> error $ "variabeln " ++ var ++ " finns inte"
      [var1, op, var2] -> case (M.lookup var1 vars, M.lookup var2 vars) of
        (Just vs1, Just vs2) -> zipWith (operator op) vs1 vs2
        (Nothing, _)         -> error $ "variabeln " ++ var1 ++ " finns inte"
        (_, Nothing)         -> error $ "variabeln " ++ var2 ++ " finns inte"
      _ -> error $ "ogiltigt uttryck: " ++ unwords expr
    vars' = M.insert name vals vars
  in runPeriod n inputs input output vars' ls
  | ["period", start, end] <- l
  = let
    a = read start
    b = read end
    (preInputs, subInputs) = span ((< a) . fst) inputs
    preOutputs = map (\(y, i) -> (y, i,) $ computeGraph i) preInputs
    input' = snd $ last preInputs
    output' = thrd $ last preOutputs
    vars' = fmap (drop (length preInputs)) vars
    (subLs, postLs) = span ((== "|") . head) ls
    n' = length $ takeWhile ((< b) . fst) subInputs
    subLs' = filter (not . null) $ map tail subLs
    (outputs, vars'') = runPeriod n' subInputs input' output' vars' subLs'
    (subOutputs, outputs') = span ((< b) . frst) outputs
    input'' = scnd $ last subOutputs
    output'' = thrd $ last subOutputs
    vars''' = fmap (drop (length subOutputs)) vars''
    postInputs = map (\(y, i, _) -> (y, i)) outputs'
    n'' = length postInputs
    (postOutputs, vars'''') = runPeriod n'' postInputs input'' output'' vars''' postLs
  in (preOutputs ++ subOutputs ++ postOutputs, vars'''')
  | otherwise = error $ "ogilitgt periodkommando: " ++ unwords l
runPeriod _ inputs _ _ vars [] = (,vars) $ map (\(y, i) -> (y, i,) $ computeGraph i) inputs

-- | Tar första elementet i en 3-tuple.
frst :: (a, b, c) -> a
frst (a, _, _) = a

-- | Tar andra elementet i en 3-tuple.
scnd :: (a, b, c) -> b
scnd (_, b, _) = b

-- | Tar tredje elementet i en 3-tuple.
thrd :: (a, b, c) -> c
thrd (_, _, c) = c

-- | Beräknar ett värde efter att det omvandlats enligt en given väg.
convertAmount :: [Node] -> InputGraph -> Amount -> Amount
convertAmount [_] _ amount = amount
convertAmount (from : to : nodes) input amount
  | Just dat <- M.lookup (from, to) $ edges input
  = let
    amount' = amount * conversion dat * transmission dat
  in convertAmount (to : nodes) input amount'
  | otherwise = error $ "kanten " ++ show (from, to) ++ " finns inte"
convertAmount [] _ _ = error "för få noder för att konvertera"

-- | Sätter en kants värde till beräknade värdet i en graf.
fixEdge :: Edge -> OutputGraph -> InputGraph -> InputGraph
fixEdge edge output input
  | Just dat <- M.lookup edge $ edges input
  , Just (x, y) <- M.lookup edge $ outputEdges output
  = let
    dat' = dat { distribution = Fixed x y }
    edges' = M.insert edge dat' $ edges input
  in input { edges = edges' }
  | otherwise = error $ "kanten " ++ show edge ++ " existerar inte"

-- | Markerar att en kant ska stå för allt resterande behov för en nod.
fillEdge :: Edge -> InputGraph -> InputGraph
fillEdge edge input
  | Just dat <- M.lookup edge $ edges input
  = let
    dat' = dat { distribution = Fill 0 1 }
    edges' = M.insert edge dat' $ edges input
  in input { edges = edges' }
  | otherwise = error $ "kanten " ++ show edge ++ " existerar inte"

-- | Översätter en representation av en operation till motsvarande funktion.
operator :: String -> (Double -> Double -> Double)
operator "+" = (+)
operator "-" = (-)
operator "*" = (*)
operator "/" = (/)
operator "=" = const
operator op = error $ "ogiltig operator: " ++ op

-- | Modifierar alla behov.
adjustDemands :: (Double -> Double) -> InputGraph -> InputGraph
adjustDemands f input = input { demands = f <$> demands input }

-- | Modifierar ett givet behov.
adjustDemand :: (Double -> Double) -> Node -> InputGraph -> InputGraph
adjustDemand f node input = input { demands = M.adjust f node $ demands input }

-- | Modifierar en given kapacitet.
adjustCapacity :: (Double -> Double) -> Node -> InputGraph -> InputGraph
adjustCapacity f node input = input { capacities = M.adjust f node $ capacities input }

-- | Justerar utgående (och ingående) värdet för en kant om den är fixerad.
adjustEdge :: (Double -> Double) -> Edge -> InputGraph -> InputGraph
adjustEdge f edge input
  | Just dat <- M.lookup edge $ edges input
  , Fixed _ y <- distribution dat
  = let
    y' = f y
    x' = y' / conversion dat / transmission dat
    dat' = dat { distribution = Fixed x' y' }
    edges' = M.insert edge dat' $ edges input
  in input { edges = edges' }

-- | Extraherar och skriver resultat till filer enligt scenariot.
processResults :: [Year] -> [OutputGraph] -> [[String]] -> IO ()
processResults years outputs (l : ls)
  | ["skriv", path] <- l
  = let
    (subLs, ls') = span ((== "|") . head) ls
    (res, names) = extractResults outputs $ map tail subLs
    res' = zipWith (:) (map show years) (map (map show) res)
    -- Byter ut '_' mot ' ' i en textsträng.
    clean = map (\c -> if c == '_' then ' ' else c)
  in do
    writeFile (path ++ ".resultat") $ unlines (map unwords res')
    writeFile (path ++ ".namn") $ unlines (map clean names)
    processResults years outputs ls'
  | otherwise = error $ "ogiltigt kommando: " ++ unwords l
processResults _ _ [] = return ()

-- | Extraherar resultat enligt scenariot.
extractResults :: [OutputGraph] -> [[String]] -> ([[Double]], [String])
extractResults outputs (l : ls)
  | ["kant", dir, from, to, opt] <- l
  , ("namn", ':' : dir') <- span (/= ':') opt
  = let
    (res, names) = extractResults outputs ls
    getVal = case dir of
      "in" -> fst
      "ut" -> snd
      _    -> error $ "ogiltigt värde från kant: " ++ dir
    extract = getVal . fromJust . M.lookup (from, to) . outputEdges
    res' = map extract outputs
    name = case dir' of
      "in" -> from
      "ut" -> to
      _    -> error $ "ogiltigt kantparameter: " ++ opt
  in (zipWith (:) res' res, name : names)
  | "noder" : nodes <- l
  = let
    (res, names) = extractResults outputs ls
    -- En function som extraherar värden från en graf för noderna.
    extract output = map (($ output) . extractNodeAmount) nodes
    res' = map extract outputs
  in (zipWith (++) res' res, nodes ++ names)
  | ["summera", name] <- l
  = let
    (subLs, ls') = span ((== "|") . head) ls
    (subRes, _) = extractResults outputs $ map tail subLs
    (res, names) = extractResults outputs ls'
    res' = map sum subRes
  in (zipWith (:) res' res, name : names)
  | "negera" : l' <- l
  = let
    (subRes, subNames) = extractResults outputs [l']
    (res, names) = extractResults outputs ls
    res' = map (map negate) subRes
  in (zipWith (++) res' res, subNames ++ names)
  | otherwise = error $ "ogiltigt kommando: " ++ unwords l
extractResults outputs [] = (replicate (length outputs) [], [])

-- | Extraherer värdet från en given nod.
extractNodeAmount :: Node -> OutputGraph -> Amount
extractNodeAmount node output
  | Just amount <- M.lookup node $ outputNodes output = amount
  | otherwise = error $ "noden " ++ node ++ " existerar inte"
