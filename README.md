# Environment Course Project

This project was done during an environment course to roughly model how the
Swedish energy system would develop under some different scenarios.

The focus was on how dismantling the Swedish nuclear energy would affect the
system. Although real numbers where used the results should probably be
disregarded completely.

## The Project Structure

The project consists of a main program performing the calculations written in
Haskell. The model and scenarios are defined in two new (simple) languages.

The project also contains three companion programs for producing latex tables, 
dot graphs and pdf graphs written in Haskell and Python.

## The Documentation

The files are all documented in Swedish, with thorough documentation in
dokumentation.txt.

## The Results

In the Output folder each generated pdf can be found. This includes the results
of the calculations as graphs and tables as well as a graph showing the model
of the energy system.

## Running the Project

The project includes the script main.sh that runs all calculations and
generates all output.

It requires that the following programs are installed:

* stack (with an appropriate default compiler)
* python (with numpy and matplotlib)
* pdflatex
* dot
* ps2pdf

