import System.Environment (getArgs)

type Node = String
type Edge = (Node, Node)

main :: IO ()
main = mapM_ makeGraph =<< getArgs

makeGraph :: String -> IO ()
makeGraph s = do
  edges <- readModell $ s ++ ".modell"
  let es = map cleanEdge edges
      dot = createGraph es
  writeFile (s ++ ".dot") dot

readModell :: FilePath -> IO [Edge]
readModell p
    = map (\(_:from:to:_) -> (from, to))
    . filter ((== "kant") . head)
    . filter (not . null)
    . map words
    . lines
  <$> readFile p

cleanEdge :: Edge -> Edge
cleanEdge (f, t) = (clean f, clean t)
  where
    clean [] = []
    clean ('_':cs) = ' ' : clean cs
    clean (c:cs) = c : clean cs

createGraph :: [Edge] -> String
createGraph es
   = "digraph Modell {\n"
  ++ "\trankdir=LR\n"
  ++ concatMap showEdge es
  ++ "}"
  where
    showEdge (from, to) = "\t\"" ++ from ++ "\" -> \"" ++ to ++ "\"\n"
