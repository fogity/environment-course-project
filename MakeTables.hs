import Data.List (intercalate)
import Numeric (showFFloat)
import System.Environment (getArgs)

main :: IO ()
main = mapM_ makeTable =<< getArgs

makeTable :: String -> IO ()
makeTable s = do
  names   <- readNames   $ s ++ ".namn"
  results <- readResults $ s ++ ".resultat"
  let res'  = takeNths 12 $ adjust results
      table = createTable ("År" : names) res'
  writeFile (s ++ ".tabular") table

readNames :: FilePath -> IO [String]
readNames = fmap lines . readFile

readResults :: FilePath -> IO [[String]]
readResults = fmap (map words . lines) . readFile

adjust :: [[String]] -> [[String]]
adjust = map (\(y:rs) -> adjustYear y : map adjustAmount rs)

adjustYear :: String -> String
adjustYear = show . (round :: Double -> Integer) . read

adjustAmount :: String -> String
adjustAmount = (flip (showFFloat (Just 2)) "" :: Double -> String) . read

takeNths :: Int -> [a] -> [a]
takeNths n = take' n
  where
    take' _ [] = []
    take' 0 xs = take' n xs
    take' k (x:xs)
      | k == n    = x : take' (k-1) xs
      | otherwise =     take' (k-1) xs

createTable :: [String] -> [[String]] -> String
createTable names results
   = "\\begin{tabular}{" ++ replicate n 'r' ++ "}\n"
  ++ header ++ end ++ "\\hline\n"
  ++ intercalate end rows ++ "\n"
  ++ "\\end{tabular}"
  where
    n = length names
    end = " \\\\\n"
    header = intercalate " & " names
    rows = map (intercalate " & ") results
