from cycler import cycler
import numpy as np
import matplotlib.pyplot as plt
import sys

def plot(path):
    A = np.loadtxt(path + '.resultat')

    with open(path + '.namn') as f:
        L = f.read().decode('utf-8').splitlines()

    fig = plt.figure(figsize=(15, 10))
    ax = plt.subplot()

    colors = plt.cm.nipy_spectral(np.linspace(0, 1, len(L)))
    ax.set_prop_cycle(cycler('color', colors))
    
    ax.plot(A[:, 0], A[:, 1:])

    ax.grid(which='major', linestyle='--', linewidth=0.5)

    box = ax.get_position()
    ax.set_position([box.x0, box.y0, 0.8 * box.width, box.height])

    ax.legend(L, loc='upper left', bbox_to_anchor=(1, 1))

    fig.savefig(path + '.pdf', format='pdf')

    plt.close()

for path in sys.argv[1:]:
    plot(path)
