#!/bin/bash

# Beräknar alla scenarier.
stack runghc Main.hs scenario1 scenario2 scenario3

# Skapar alla scenario-grafer.
python make_simple_graphs.py scenario1_energiproduktion_graf scenario2_energiproduktion_graf scenario3_energiproduktion_graf

# Skapar alla scenario-tabeller.
stack runghc MakeTables.hs scenario1_energiproduktion_tabell scenario2_energiproduktion_tabell scenario3_energiproduktion_tabell

# Skapar en pdf för att lättare läsa tabellerna (ej med i rapporten).
pdflatex tabeller.tex tabeller.pdf

# Skapar en graf över modellen (något förenklad version).
stack runghc MakeDotFiles.hs simple
dot -Tps2 -o simple.ps2 simple.dot
ps2pdf simple.ps2 simple.pdf
